## Adminstrative project

An administrative project helping [DTU](https://www.dtu.dk) researchers to create and/or share code projects under a common DTU namespace.

Please open a [new issue](https://gitlab.com/DTUDK/admin/-/issues/new) in this project for new users and creating repositories.

If you have questions; either open a issue or contact us at [mycode@dtu.dk](mailto:mycode@dtu.dk).


### Requesting a repository

Any DTU employee can get a repository.  
Open a [new issue](https://gitlab.com/DTUDK/admin/-/issues/new), choose
`New Repository` in the `Description` drop down and follow the instructions.  
Ensure you have marked the `This issue is confidential...` for privacy.

See following items for detailed information.


#### Opensource/public codes

- Obtain a [Software Notification Form](https://www.inside.dtu.dk/da/medarbejder/om-dtu-campus-og-bygninger/kommunikation-og-design/meddelelser/meddelelser_dtuansatte/meddelelsevisning?id={FA708C06-2270-423B-907D-91F1C8B7D0A3}) from [here](https://www.inside.dtu.dk/da/medarbejder/forskning-innovation-og-raadgivning/forskningssamarbejde-jura-og-kontraktforhold/om-software/software-der-ikke-oenskes-kommercialiseret), or at least discuss it with your departments innovation responsible.
- Choose a license for the software, such that you uphold the software dependent packages/libraries licenses
- Choose a unique repository name, and the location.  
  Our basic layout follows `Institute/Group/Project`; ask for other group structures
- A list of DTU users that will gain access.  
  Please designate at least 1 maintainer. This maintainer can invite members at will.
- In case the software has significant amount of data, we encourage the use of [DTU Data](https://data.dtu.dk/)


#### Closed/private codes

These codes may be created with `Private` visibility level meaning that others have no access.

Since these are hidden they don't disclose any source code for other users and one need not assign
licenses. However, we still encourage as many items in the previous section.


### Adding users

Our [permission table](https://docs.gitlab.com/ee/user/permissions.html) is restrictive in the sense that we have to retain ownership of project folders due to
legal issues. Therefore users *have* to request a project before it can be created.

Our basic permissions are:

- `Developer` flags are given to members of a `Group`
- `Maintainer` flags are given to members `Institute/Group/Project`, they can themselves control access to the
  given project through the `Members` menu.

To add a new user to projects, contact the `Maintainer` of the project.  
To grant `Developer` access to a `Group` please choose `Add Users` in the `Description` drop
down of the [issue page](https://gitlab.com/DTUDK/admin/-/issues/new), please ensure you have marked the `This issue is confidential...` for privacy.


### Other requests

Please feel free to ask questions about usage and/or suggestions for improving the process flow
of this [namespace](https://gitlab.com/DTUDK). Open a [issue](https://gitlab.com/DTUDK/admin/-/issues/new) or
email us at [mycode@dtu.dk](mailto:mycode@dtu.dk).


### Who are *we*

This service is provided *as-is* and managed at the [DTU Computing Center](https://www.hpc.dtu.dk/).
Our services are guided by the researchers requests and needs.  
Please do not hesitate to contact us for guidance and/or suggestions.
